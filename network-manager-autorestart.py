"""
This scipt must be run as root to work.
"""
import os
import subprocess
import shlex
import logging
import sys
import time
import signal

# simple print-based logging.
_l = logging.getLogger(name=__name__)

_stdout_log_handler = logging.StreamHandler(sys.stdout)
_stdout_log_handler.setLevel(logging.INFO)

logging.root.setLevel(logging.INFO)
logging.root.addHandler(_stdout_log_handler)


keep_running = True


def ping_check(ip_or_url: str) -> bool:
    """
    Return True if the website is successfully pinged, else return false.
    """
    try:
        subprocess.check_call(shlex.split("ping -c 1 {}".format(ip_or_url)), stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    except subprocess.CalledProcessError as e:
        _l.warn("Ping to '{}' failed at time '{}'".format(ip_or_url, time.ctime()))
        return False
    else:
        return True

def reconnect_network_manager():
    """
    Cause the network manager to reconnect.
    """
    try:
        subprocess.call(shlex.split("nmcli networking off"), stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        subprocess.call(shlex.split("nmcli networking on"),  stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    except subprocess.CalledProcessError as e:
        _l.error("Unable to restart the networking manager, is this program running as root as required?")

def term_handler(signum, frame):
    """
    Handler for sigterm.
    """
    global keep_running
    keep_running = False

signal.signal(signal.SIGTERM, term_handler)

if __name__ == "__main__":
    while keep_running:
        is_connected = ping_check("www.google.com")
        if not is_connected:
            reconnect_network_manager()
        time.sleep(20)
