#!/usr/bin/bash

# From: https://stackoverflow.com/questions/59895/getting-the-source-directory-of-a-bash-script-from-within
SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"

# Create the script that starts the networkmanager restarted.
echo "Creating script that starts the NetworkManager connectivity restarter watchdog..."

echo "#!/usr/bin/sh" > /usr/bin/networkmanager-auto-restart-service.sh
echo "cd \"$DIR\";" >> /usr/bin/networkmanager-auto-restart-service.sh

# The & means this process forks.
echo "python3 \"$DIR/network-manager-autorestart.py\" &" >> /usr/bin/networkmanager-auto-restart-service.sh
chmod +x /usr/bin/networkmanager-auto-restart-service.sh


